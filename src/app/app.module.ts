import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { PasswordComponent } from './components/auth/password/password.component';
import { CreateuserComponent } from './components/users/user/createuser/createuser.component';
import { ListuserComponent } from './components/users/user/listuser/listuser.component';
import { CreateprofileComponent } from './components/users/profiles/createprofile/createprofile.component';
import { ListprofileComponent } from './components/users/profiles/listprofile/listprofile.component';
import { CreateclientComponent } from './components/home/admin/client/createclient/createclient.component';
import { ListclientComponent } from './components/home/admin/client/listclient/listclient.component';
import { CreateparameterComponent } from './components/home/admin/parameter/createparameter/createparameter.component';
import { ListparameterComponent } from './components/home/admin/parameter/listparameter/listparameter.component';
import { CreateproductComponent } from './components/home/admin/products/createproduct/createproduct.component';
import { ListproductComponent } from './components/home/admin/products/listproduct/listproduct.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PasswordComponent,
    CreateuserComponent,
    ListuserComponent,
    CreateprofileComponent,
    ListprofileComponent,
    CreateclientComponent,
    ListclientComponent,
    CreateparameterComponent,
    ListparameterComponent,
    CreateproductComponent,
    ListproductComponent,
    NavbarComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
