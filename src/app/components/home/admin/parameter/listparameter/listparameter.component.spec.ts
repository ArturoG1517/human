import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListparameterComponent } from './listparameter.component';

describe('ListparameterComponent', () => {
  let component: ListparameterComponent;
  let fixture: ComponentFixture<ListparameterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListparameterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListparameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
