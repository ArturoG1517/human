import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateparameterComponent } from './createparameter.component';

describe('CreateparameterComponent', () => {
  let component: CreateparameterComponent;
  let fixture: ComponentFixture<CreateparameterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateparameterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateparameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
